#!/usr/bin/env python3
# coding: utf-8

import sys

from mcbot import SakuraBot


def main():
    username = sys.argv[1]
    passwd = input("passwd:") if len(sys.argv) < 3 else sys.argv[2]

    conf = {
        'username': username,
        'passwd': passwd,
        'dump_packets': False,
    }

    bot = SakuraBot(**conf)
    bot.connect()
    bot.loop()
    print("now exit")

if '__main__' == __name__:
    main()
