#!/usr/bin/env python3

import sys
import fn
import re
import traceback

from minecraft.networking.connection import Connection
from minecraft.networking.packets import Packet, clientbound, serverbound

from minecraft.compat import input

from mcbot import CoreBot

from pprint import pprint as pp

_color_wipe_pattern = re.compile('§[0-9a-fklmnor]', re.U|re.I|re.M|re.S)
_color_wipe = lambda s: re.sub(_color_wipe_pattern, '', s)

STATES = {
    'INIT': 0,
    'CONNECTED': 1,
    'AUTHED': 2,
    'AUTHFAIL': 3,
}

def chat_strip(obj):
    if 'extra' not in obj:
        return (_color_wipe(obj.get('text', repr(obj))), )
    else:
        return fn.iters.flatten((_color_wipe(obj.get('text', repr(obj))), ) + tuple(chat_strip(o) for o in obj['extra']))


class SakuraRegBot(CoreBot):
    def __init__(self, **kwargs):
        kwargs.update({'address': 'sakuratown.cn', 'port': 25565})
        super().__init__(**kwargs)

    def handle_login_state(self, pos, msg):
        if pos == 'SYSTEM' and self.state == STATES['CONNECTED']:
            if '该账号暂未注册' in msg:
                packet = serverbound.play.ChatPacket()
                packet.message = '/reg {}'.format(self.passwd)
                self.connection.write_packet(packet)

        if pos == 'CHAT' and self.state == STATES['CONNECTED']:
            if '成功登录樱花镇' in msg:
                self.state = STATES['AUTHED']
                self.connection.disconnect()
                self.keepon = False

def main():
    username = sys.argv[1]
    passwd = input("passwd:") if len(sys.argv) < 3 else sys.argv[2]

    conf = {
        'username': username,
        'passwd': passwd,
        'dump_packets': True,
    }

    bot = SakuraRegBot(**conf)
    bot.connect()
    bot.loop()
    print("now exit")

if '__main__' == __name__:
    main()
