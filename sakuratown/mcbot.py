#!/usr/bin/env python3

import sys
import signal
import json
import fn
import re
import time
import math
import traceback

from minecraft import authentication
from minecraft.exceptions import YggdrasilError
from minecraft.networking.connection import Connection
from minecraft.networking.packets import Packet, clientbound, serverbound

from minecraft.compat import input

from pprint import pprint as pp

_color_wipe_pattern = re.compile('§[0-9a-fklmnor]', re.U|re.I|re.M|re.S)
_color_wipe = lambda s: re.sub(_color_wipe_pattern, '', s)
_nick_prefix_wipe = lambda s: re.sub(r'\[[^]]+\]|\s+', '', s)

_cmd_split = re.compile(r'\s+', re.U|re.I|re.M|re.S)

STATES = {
    'INIT': 0,
    'CONNECTED': 1,
    'AUTHED': 2,
    'AUTHFAIL': 3,
}

todayfmt = lambda : time.strftime('%Y%m%d', time.localtime())

class TimeoutExpired(Exception):
    pass

def chat_strip(obj):
    if 'extra' not in obj:
        return (_color_wipe(obj.get('text', repr(obj))), )
    else:
        return fn.iters.flatten((_color_wipe(obj.get('text', repr(obj))), ) + tuple(chat_strip(o) for o in obj['extra']))

def input_with_timeout(prompt, timeout):
    def alarm_handler(signum, frame):
        raise TimeoutExpired

    signal.signal(signal.SIGALRM, alarm_handler)
    signal.alarm(timeout) # produce SIGALRM in `timeout` seconds

    try:
        return input(prompt)
    finally:
        signal.alarm(0) # cancel alarm

class CoreBot(object):
    ignored_types = {
        Packet,

        ##clientbound.play.ChatMessagePacket,
        clientbound.play.PlayerListItemPacket,
        clientbound.play.BlockChangePacket,
        clientbound.play.MultiBlockChangePacket,
        clientbound.play.KeepAlivePacket,
        clientbound.play.PlayerPositionAndLookPacket,
        clientbound.play.EntityVelocityPacket,
        clientbound.play.SpawnObjectPacket,

        serverbound.play.KeepAlivePacket,
        serverbound.play.TeleportConfirmPacket,
    }

    def __init__(self, username, passwd, address, port=25565, password=None, offline=True, dump_packets=False, **kwargs):
        self.username = username
        self.passwd = passwd
        self.keepon = False

        if offline:
            print("Connecting in offline mode...")
            self.connection = Connection(address, port, username=username, **kwargs)
        else:
            auth_token = authentication.AuthenticationToken()

            try:
                auth_token.authenticate(username, password)
            except YggdrasilError as e:
                print(e)
                sys.exit()

            print("Logged in as %s..." % auth_token.username)
            self.connection = Connection(address, port, auth_token=auth_token)

        if dump_packets:
            self.connection.register_packet_listener(self.handle_incoming, Packet, early=True)
            self.connection.register_packet_listener(self.handle_outgoing, Packet, outgoing=True)

        self.connection.register_packet_listener(self.handle_join_game, clientbound.play.JoinGamePacket)
        self.connection.register_packet_listener(self.handle_chat_message, clientbound.play.ChatMessagePacket)
        self.connection.register_packet_listener(self.handle_player_list_item, clientbound.play.PlayerListItemPacket)
        self.connection.register_packet_listener(self.handle_spawn_object, clientbound.play.SpawnObjectPacket)
        self.connection.register_packet_listener(self.handle_player_position_and_look, clientbound.play.PlayerPositionAndLookPacket)

        self.players = dict()

        self.objects = dict()

        self.state = STATES['INIT']
        self.pos_look = clientbound.play.PlayerPositionAndLookPacket.PositionAndLook()

    def handle_player_position_and_look(self, packet):
        packet.apply(self.pos_look)

    def handle_incoming(self, packet):
        ptype = type(packet)
        if any(ptype is p for p in self.ignored_types):
            return

        print('--> %s' % packet, file=sys.stderr)

    def handle_outgoing(self, packet):
        ptype = type(packet)
        if any(ptype is p for p in self.ignored_types):
            return

        print('<-- %s' % packet, file=sys.stderr)

    def handle_join_game(self, join_game_packet):
        print('Connected.', join_game_packet, file=sys.stderr)
        self.state = STATES['CONNECTED']

    def handle_chat_message(self, packet):
        pos = packet.field_string('position')
        jdata = json.loads(packet.json_data)

        if pos == 'SYSTEM':
            translate = jdata.get('translate', '')
            if translate.startswith('death.'):
                return    ## ignore some notifications
            if translate in ('chat.type.advancement.task', ):
                return

        gmsg = [m for m in chat_strip(jdata) if m]
        msg = ''.join(gmsg)
        ##print(json.dumps([m for m in chat_strip(jdata) if m], indent=2))

        if pos == 'SYSTEM' and ('请求你传送到他那里' in msg or '请求传送到你这里' in msg):
            self.handle_tp_request(gmsg, msg)
            return

        if self.state != STATES['AUTHED']:
            self.handle_login_state(pos, msg)
            return

        else:
            if len(gmsg) == 4 and '悄悄地对你说' in gmsg[1]:
                self.handle_priv_message(gmsg[0].strip(), gmsg[3].strip())
                return

        print('Msg [%s](%s): %s'%(self.state, pos, msg))

    def handle_player_list_item(self, packet):
        subcls = {
            clientbound.play.PlayerListItemPacket.AddPlayerAction: 'add-player',
            clientbound.play.PlayerListItemPacket.UpdateGameModeAction: 'update-gamemode',
            clientbound.play.PlayerListItemPacket.UpdateLatencyAction: 'update-latency',
            clientbound.play.PlayerListItemPacket.UpdateDisplayNameAction: 'update-displayname',
            clientbound.play.PlayerListItemPacket.RemovePlayerAction: 'remove-player'
        }

        for action in packet.actions:
            typ = subcls.get(type(action), None)

            if typ == 'add-player':
                uuid = action.uuid
                name = ''.join(chat_strip(json.loads(action.display_name))) if action.display_name else None
                if not name: continue
                if ' BTLP ' in action.name: continue ## ignore
                if uuid in self.players: continue
                info = {k:getattr(action, k) for k in ('name', 'display_name', 'ping')}
                self.players[uuid] = info

            elif typ == 'update-latency':
                uuid = action.uuid
                info = self.players.get(uuid, None)
                if info is None: continue
                info['ping'] = action.ping

            elif typ == 'update-displayname':
                uuid = action.uuid
                if uuid not in self.players: continue

                name = ''.join(chat_strip(json.loads(action.display_name))) if action.display_name else None
                if not name: continue

                info = self.players[uuid]
                info['display_name'] = name

            elif typ == 'remove-player':
                uuid = action.uuid
                if uuid not in self.players: continue
                del self.players[uuid]

            elif typ == 'update-gamemode':
                pass        ## nonesense, unless you want to wait for admin's fault

            else:
                info = {k:getattr(action, k) for k in action.__slots__} if type(action.__slots__) is tuple else {action.__slots__:getattr(action, action.__slots__)}
                info.update({'type_name': typ})
                print(info, file=sys.stderr)

    def handle_spawn_object(self, packet):
        fields = ('entity_id', 'x', 'y', 'z', 'type', 'data', )
        uuid = packet.object_uuid
        info = {field:getattr(packet, field) for field in fields}
        info.update({'uuid': uuid})

        if uuid in self.objects:
            del self.objects[uuid]

        self.objects[uuid] = info

        print(info, file=sys.stderr)

    def send_msg(self, msg):
        packet = serverbound.play.ChatPacket()
        packet.message = msg
        self.connection.write_packet(packet)

    def connect(self):
        self.connection.connect()

    def loop(self):
        self.keepon = True
        while self.keepon:
            try:
                text = input_with_timeout(">> ", 30)
                if text.startswith('.'):
                    text = text[1:]
                    parts = re.split(r'\s+', text)
                    cmd, args = parts[0], parts[1:]

                    func = getattr(self, 'command_{}'.format(cmd), lambda *aargs: print('unrecognized command => .{}'.format(text)))
                    try:
                        func(*args)
                    except:
                        traceback.print_exc()

                    continue

                if not text: continue
                self.send_msg(text)
            except TimeoutExpired:
                continue
            except KeyboardInterrupt:
                self.connection.disconnect()
                self.keepon = False

class SakuraBot(CoreBot):
    def __init__(self, **kwargs):
        kwargs.update({
                        'address': 'sakuratown.cn',
                        'port': 25565,
                        'handle_exception': self.handle_exception,
                        'handle_exit': self.handle_exit,
        })
        self.admins = set()
        self.username = kwargs['username']
        super().__init__(**kwargs)

    def handle_login_state(self, pos, msg):
        if pos == 'SYSTEM' and self.state == STATES['CONNECTED']:
            if '请按回车输入' in msg and '进行登录' in msg:
                packet = serverbound.play.ChatPacket()
                packet.message = '/l {}'.format(self.passwd)
                self.connection.write_packet(packet)

        if pos == 'CHAT' and self.state == STATES['CONNECTED']:
            if '您已成功登录樱花镇服务器' in msg:
                self.state = STATES['AUTHED']

    def handle_tp_request(self, gmsg, msg):
        idxs = [idx for idx, m in enumerate(gmsg) if '传送' in m][0]
        from_user = _nick_prefix_wipe(''.join(gmsg[:idxs]))

        if from_user in self.admins:
            self.send_msg('/tpaccept')
        else:
            self.send_msg('/tpdeny')

    def handle_priv_message(self, from_user, msg):
        if not msg.startswith('.'):
            print('PrivMsg <{}>: {}'.format(from_user, msg))
            return

        args = _cmd_split.split(msg)
        cmd = args[0][1:]
        params = args[1:]

        if cmd == 'auth':
            if params[0] == '{}{}'.format(self.username, todayfmt()):
                self.admins.add(from_user)
                self.send_msg('/msg {} {}'.format(from_user, '校验通过 你可以控制我啦'))
            else:
                self.send_msg('/msg {} {}'.format(from_user, '嘿嘿'))
        else:
            if from_user not in self.admins:
                self.send_msg('/msg {} {}'.format(from_user, '嘿嘿'))
            else:
                func = getattr(self, 'command_{}'.format(cmd), lambda *aargs: print('unrecognized command => .{}'.format(text)))
                try:
                    func(*params, from_user=from_user)
                except:
                    traceback.print_exc()
                    self.send_msg('/msg {} {}'.format(from_user, '出了点小问题啊 大佬'))

    def handle_exception(self, *args, **kwargs):
        print("!IMPORT: Exception", args, kwargs)
        traceback.print_exc()

    def handle_exit(self, *args, **kwargs):
        self.connection.disconnect()
        self.keepon = False

    def command_admins(self, *args, from_user=None):
        sleep_int = 0.001 if from_user is None else 0.2
        sendback = print if from_user is None else lambda m: self.send_msg('/msg {} {}'.format(from_user, m))
        for u in self.admins:
            sendback(u)
            time.sleep(sleep_int)

    def command_players(self, *args, from_user=None):
        infos = ["{name}:{ping}".format(**i) for i in sorted(self.players.values(), key=lambda v: v['ping'])]
        pp(infos)

    def command_say(self, *args, from_user=None):
        self.send_msg(' '.join(args))
        if from_user is not None:
            self.send_msg('/msg {} 执行成功'.format(from_user))

    def command_download_maps(self, *args, from_user=None):
        packet = serverbound.play.ClientSettingsPacket()
        packet.locale = '简体中文'
        packet.view_distance = 5
        packet.chat_mode = 0,
        packet.chat_colors = False,

        self.connection.write_packet(packet)

    def command_walk(self, *args, from_user=None):
        steps = int(args[0])
        steps = 100 if steps > 100 else steps

        for i in range(steps):
            self.pos_look.yaw = (self.pos_look.yaw + 1.0) % 360.0
            # Move forward 0.1m.
            self.pos_look.x -= 0.1 * math.sin(self.pos_look.yaw * math.pi / 180.0)
            self.pos_look.z += 0.1 * math.cos(self.pos_look.yaw * math.pi / 180.0)

            self.connection.write_packet(serverbound.play.PositionAndLookPacket(
                x         = self.pos_look.x,
                feet_y    = self.pos_look.y,
                z         = self.pos_look.z,
                yaw       = self.pos_look.yaw,
                pitch     = self.pos_look.pitch,
                on_ground = True))

            time.sleep(0.5)
